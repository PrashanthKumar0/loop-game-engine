#include <glad/glad.h>

#include <vector>
#include <tuple>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "LoopEngine/Mesh.hpp"
#include "LoopEngine/Window.hpp"
#include "LoopEngine/Shader.hpp"
#include "LoopEngine/Circle.hpp"
#include "LoopEngine/constants.hpp"
#include "LoopEngine/ShadersCollection.hpp"


// helper function
static std::tuple<std::vector<GLfloat>, std::vector<GLuint> > genCircle(float radius);



// public:
le::Circle::Circle(float radius)
    : m_radius(radius)
{
    auto [vertices, indices] = genCircle(radius);

    m_mesh = std::make_shared<le::Mesh>(vertices, indices);
    m_shader = std::make_shared<le::Shader>(le::ShadersCollection::colorVS, le::ShadersCollection::colorFS);
}

// public:

void le::Circle::draw(const le::Window &window)
{
    // TODO : optimize drawing and move this somewhere else
    glm::mat4 model{glm::translate({1.0f}, glm::vec3{m_position.x, m_position.y -  window.getHeight(), 0.0f})};
    glm::mat4 view{glm::lookAt(glm::vec3{0.0f, 0.0f, -1.0f}, glm::vec3{0.0f}, glm::vec3{0.0f, -1.0f, 0.0f})};
    glm::mat4 projection{glm::ortho(0.0f, window.getWidth(), 0.0f, window.getHeight(), -100.0f, 100.0f)};
    glm::mat4 mvp{projection * view * model};
    glm::vec4 color{m_color.r / 255.0f, m_color.g / 255.0f, m_color.b / 255.0f, m_color.a / 255.0f};

    m_shader->use();
    m_shader->set("MVP", mvp);
    m_shader->set("uColor", color);
    m_mesh->draw();
}

void le::Circle::setColor(const le::Color &color)
{
    m_color = color;
}

void le::Circle::setPosition(float x, float y)
{
    m_position = {x, y};
}


// helper

static std::tuple<std::vector<GLfloat>, std::vector<GLuint> > genCircle(float radius)
{
    std::vector<GLfloat> vertices{
        0.0f, 0.0f // center
    };
 
    std::vector<GLuint> indices{};

    // 3 pixel arc
    // => 3 * n = 2 * PI * r
    // => n = 2 * PI * r / 3
    float resolution{3.0f}; // less the better
    int numPoints{static_cast<int>((2.0f * le::constants::pi * radius) / resolution)};
    float rotateAngle{resolution / radius};
    glm::vec4 vec{radius, 0.0f, 0.0f, 1.0f};
    glm::mat4 rot{glm::rotate(glm::mat4{1.0f}, rotateAngle, glm::vec3{0.0f, 0.0f, 1.0f})};
    
    while(numPoints--)
    {
        vertices.push_back(vec.x);
        vertices.push_back(vec.y);
        vec = rot * vec;
    }

    vertices.push_back(radius);
    vertices.push_back(0.0f);


    for(int i{1}; i <= (vertices.size() / 2 - 1); i++)
    {
        indices.push_back(0);
        indices.push_back(i);
        indices.push_back(i+1);
    }


    return {vertices, indices};
}

