#include <glad/glad.h>
#include <glm/glm.hpp>

#include <string>
#include <stdexcept>

#include "LoopEngine/Logger.hpp"
#include "LoopEngine/Shader.hpp"

// TODO : move somewhere else ?
GLuint createShader(const char* shaderSource, GLenum shaderType);



le::Shader::Shader(const char* vertShaderSource, const char* fragShaderSource)
{
    GLuint vs{createShader(vertShaderSource, GL_VERTEX_SHADER)};
    GLuint fs{createShader(fragShaderSource, GL_FRAGMENT_SHADER)};
    
    m_programID = glCreateProgram();

    glAttachShader(m_programID, vs);
    glAttachShader(m_programID, fs);
    glLinkProgram(m_programID);

    GLint status{};
    glGetProgramiv(m_programID, GL_LINK_STATUS, &status);

    if(!status)
    {
        char infoLog[512]{};
        glGetProgramInfoLog(m_programID, 512, nullptr, infoLog);

        glDeleteShader(vs);
        glDeleteShader(fs);
        glDeleteProgram(m_programID);

        throw std::runtime_error("Failed linking shaders " + std::string(infoLog));
    }

    glDeleteShader(vs);
    glDeleteShader(fs);
}

le::Shader::~Shader()
{
    LOG("Destroy Shader");
    glDeleteProgram(m_programID);
}

void le::Shader::use() const
{
    glUseProgram(m_programID);
}

void le::Shader::set(const std::string& name, const glm::mat4& val) const
{
    GLint location{glGetUniformLocation(m_programID, name.c_str())};
    glUniformMatrix4fv(location, 1, GL_FALSE, &val[0][0]);
}

void le::Shader::set(const std::string& name, const glm::vec4& val) const
{
    GLint location{glGetUniformLocation(m_programID, name.c_str())};
    glUniform4fv(location, 1, &val[0]);
}



GLuint createShader(const char* shaderSource, GLenum shaderType)
{
    GLuint shaderID{glCreateShader(shaderType)};
    
    glShaderSource(shaderID, 1, &shaderSource, nullptr);
    glCompileShader(shaderID);


    GLint status{};

    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &status);

    if(!status)
    {
        char infoLog[512];
        glGetShaderInfoLog(shaderID, 512, nullptr, infoLog);
        throw std::runtime_error("Shader failed compiling " + std::string(infoLog));
    }

    return shaderID;
}
