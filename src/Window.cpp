#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <string>
#include <stdexcept>

#include "LoopEngine/Window.hpp"
#include "LoopEngine/Color.hpp"
#include "LoopEngine/Keys.hpp"





le::Window::Window (int width, int height,const std::string& title)
    : m_width(width)
    , m_height(height)
    , m_title(title)
{

    if(glfwInit() != GLFW_TRUE)
    {
        throw std::runtime_error("glfw Failed initializing!");
    }

    // opengl 3.3 core
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    m_glfwWindow = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);

    glfwMakeContextCurrent(m_glfwWindow);


    if(!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress))
    {
        throw std::runtime_error("glad failed loading opengl shared lib");
    }

    glViewport(0, 0, width, height);

    m_isRunning = true;
}

le::Window::~Window ()
{
    glfwDestroyWindow(m_glfwWindow);
}


bool le::Window::isRunning() const
{
    return m_isRunning && !glfwWindowShouldClose(m_glfwWindow);
}

void le::Window::clear(const le::Color& color)
{
    glClearColor(color.r / 255.0f, color.g / 255.0f, color.b / 255.0f, color.a / 255.0f);
    glClear(GL_COLOR_BUFFER_BIT);
}

void le::Window::close()
{
    glfwSetWindowShouldClose(m_glfwWindow, true);
}





bool le::Window::keyPressed(le::Keys key)
{
    if(glfwGetKey(m_glfwWindow, le::getKeymap(key)) == GLFW_PRESS)
    {
        return true;
    }

    return false;
}


void le::Window::sync()
{
    glfwPollEvents();
    glfwSwapBuffers(m_glfwWindow);
}



float le::Window::getWidth() const
{
    return m_width;
}

float le::Window::getHeight() const
{
    return m_height;
}


