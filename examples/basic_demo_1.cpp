// Required Spec
// A Basic Specification to help making lib

#include <LoopEngine/Engine.hpp>

int main()
{
    le::Window window{640, 400, "Window Title"};
    le::Circle c{100.0f};
    c.setPosition(320.0f, 200.0f);
    c.setColor(le::Color(0, 0, 255));

    // le::Rectangle r{200, 200, 100, 100};
    // r.setPosition(0, 0);
    // r.setColor(le::Color(0, 255, 255));
    float x{0.0f};
    float y{0.0f};
    while(window.isRunning())
    {
        window.clear(le::Color(0, 0, 0));
        c.setPosition(x, y);
        c.draw(window);
        
        x += 0.6f;
        y += 0.6f;

        if(x > window.getWidth() + 100.0f)
        {
            x = -100.0f;
        }
        
        if(y > window.getHeight() + 100)
        {
            y = -100.0f;
        }


        // r.draw(window);
        if(window.keyPressed(le::Keys::Escape))
        {
            window.close();
        }
        window.sync();
    }
}