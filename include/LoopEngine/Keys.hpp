#ifndef _LOOP_ENGINE_KEY_HPP_
#define _LOOP_ENGINE_KEY_HPP_


namespace le{

    // TODO :: add more keys 
    enum class Keys{
        Escape,

        //------
        Invalid
    };


    //--------------------------------------

    // maps our api to glfw api keys
    static int getKeymap(Keys key)
    {
        switch (key)
        {
        case Keys::Escape:
            return GLFW_KEY_ESCAPE;
        
        default:
            return -1;
        }
    }



}


#endif // _LOOP_ENGINE_KEY_HPP_