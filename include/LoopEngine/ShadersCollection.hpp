#ifndef _LOOP_ENGINE_SHADERS_COLLECTION_HPP_
#define _LOOP_ENGINE_SHADERS_COLLECTION_HPP_


namespace le
{


    struct ShadersCollection
    {

        //------------------------------
        static constexpr char * colorVS {
R"colorVS(

#version 330 core
layout(location=0) in vec2 aPosition;

uniform mat4 MVP;

void main()
{
    gl_Position = MVP * vec4(aPosition, 0.5f, 1.0f);
}


)colorVS"
        };


        //------------------------------
        static constexpr char * colorFS {
R"colorFS(

#version 330 core
uniform vec4 uColor;

out vec4 fragColor;

void main()
{

    fragColor = uColor;
}


)colorFS"
        };


        //------------------------------


    }; // class ShadersCollection




} // namespace le



#endif // _LOOP_ENGINE_SHADERS_COLLECTION_HPP_