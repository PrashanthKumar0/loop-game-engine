#ifndef _LOOP_ENGINE_CIRCLE_HPP
#define _LOOP_ENGINE_CIRCLE_HPP


#include <memory>

#include <glm/glm.hpp>

#include "LoopEngine/Shape.hpp"
#include "LoopEngine/Mesh.hpp"
#include "LoopEngine/Shader.hpp"
#include "LoopEngine/Window.hpp"


// TODO : Improve circle rendering with maybe SDF I've cluttered up things here


namespace le
{

    class Circle : Shape
    {

        public:
            Circle(float radius);

        public:
            auto draw(const Window& window) ->      void override;
            auto setColor(const Color& color) ->    void override;
            auto setPosition(float x, float y) ->   void override;

        private:
            glm::vec2 m_position{};
            Color m_color{};
            float m_radius{};

            std::shared_ptr<Mesh> m_mesh{};
            std::shared_ptr<Shader> m_shader{};
    };

}


#endif // _LOOP_ENGINE_CIRCLE_HPP