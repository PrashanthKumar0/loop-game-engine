#ifndef __LOGGER___HPP__
#define __LOGGER___HPP__

#include <iostream>

#define LOG(x) std::cout << x << '\n';

#endif // __LOGGER___HPP__