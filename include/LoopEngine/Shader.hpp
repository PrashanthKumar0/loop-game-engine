#ifndef _LOOP_ENGINE_SHADER_HPP_
#define _LOOP_ENGINE_SHADER_HPP_

#include <string>

#include <glad/glad.h>
#include <glm/glm.hpp>

namespace le
{


    class Shader
    {
        public:
            Shader(const char* vertShaderSource, const char* fragShaderSource);
            ~Shader();

        public:
            auto use() const -> void;
            auto set(const std::string& name, const glm::mat4& val) const -> void;
            auto set(const std::string& name, const glm::vec4& val) const -> void;

        private:
            GLuint m_programID{};
    }; // class Shader




} // namespace le





#endif // _LOOP_ENGINE_INTERNAL_SHADER_HPP_