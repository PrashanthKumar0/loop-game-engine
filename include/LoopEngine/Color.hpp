#ifndef _LOOP_ENGINE_COLOR_HPP_
#define _LOOP_ENGINE_COLOR_HPP_

namespace le 
{

    // RGBA color
    struct Color{
        public:

            Color() = default;

            Color(int _r, int _g, int _b) 
            : r(_r) , g(_g) , b(_b)
            {}

            Color(int _r, int _g, int _b, int _a) 
            : r(_r) , g(_g) , b(_b) , a(_a)
            {}

        public:
            int r{0};
            int g{0};
            int b{0};
            int a{255};
    };

} // namespace le 

#endif // _LOOP_ENGINE_COLOR_HPP_