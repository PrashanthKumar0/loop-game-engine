#ifndef __CONSTANTS__HPP__
#define __CONSTANTS__HPP__

namespace le
{
    struct constants
    {
        static constexpr float pi{3.14159265359};
    };
}

#endif // __CONSTANTS__HPP__