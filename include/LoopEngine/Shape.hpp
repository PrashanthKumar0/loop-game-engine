#ifndef _LOOP_ENGINE_SHAPE_HPP_
#define _LOOP_ENGINE_SHAPE_HPP_

#include "LoopEngine/Color.hpp"
#include "LoopEngine/Window.hpp"

namespace le
{
    class Window;
    
    class Shape
    {
        public:
            virtual auto draw(const Window& window) -> void = 0;
            virtual auto setColor(const Color& color) -> void = 0;
            virtual auto setPosition(float x, float y) -> void = 0;
        
    };
}

#endif // _LOOP_ENGINE_SHAPE_HPP_