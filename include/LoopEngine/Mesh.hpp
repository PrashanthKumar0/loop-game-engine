#ifndef _LOOP_ENGINE_MESH_HPP_
#define _LOOP_ENGINE_MESH_HPP_

#include <vector>
#include <glad/glad.h>

namespace le
{


    class Mesh
    {
        public:
            Mesh(const std::vector<GLfloat>& vertices, const std::vector<GLuint>& indices);
            ~Mesh();

        public:
            auto draw() -> void;

        private:
            std::size_t m_numIndices{};
            GLuint m_VAO{};
            GLuint m_VBO{};
            GLuint m_EBO{};
    }; // class Mesh


} // namespace le



#endif // _LOOP_ENGINE_INTERNAL_MESH_HPP_