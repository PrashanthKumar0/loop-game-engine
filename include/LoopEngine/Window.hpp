#ifndef _LOOP_ENGINE_WINDOW_HPP_
#define _LOOP_ENGINE_WINDOW_HPP_


#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <string>

#include "LoopEngine/Color.hpp"
#include "LoopEngine/Shape.hpp"
#include "LoopEngine/Keys.hpp"


namespace le
{

    class Window
    {
    public:
        Window (int width, int height,const std::string& title);
        ~Window();
    public:
        auto sync() ->                      void;
        auto close() ->                     void;
        auto isRunning() const ->           bool;
        auto keyPressed(Keys key) ->        bool;
        auto clear(const Color& color) ->   void;
        auto getWidth() const ->            float;
        auto getHeight() const ->           float;

    private:
        int m_width{};
        int m_height{};
        std::string m_title{};


        GLFWwindow* m_glfwWindow{nullptr};
        bool m_isRunning{false};
    }; // class Window


} // namespace le




#endif // _LOOP_ENGINE_WINDOW_HPP_
